#!/usr/bin/env perl
use warnings;
use strict;
use v5.10;
use Term::ANSIColor;
use File::Copy 'cp';
use File::Path;
use File::Find;
use Data::Dumper;
use List::MoreUtils ':all';
use Archive::Tar;
use Cwd;
use Git::Repository;
use LWP::Simple;
use File::Fetch;

#----------------- Set variables -----------------------#
my ( @tmpdirs, @dirs, @files );

# set defaults
my $outfile = 'shell-defaults.tar.gz';
my $basedir = 'shell-defaults';

# New datastructure!
my %Git = (
    'https://github.com/seebi/dircolors-solarized.git'    => 'home',
    'https://github.com/tpope/vim-pathogen.git'           => 'vim/autoload',
    'https://github.com/tpope/vim-sensible.git'           => 'vim/bundle',
    'https://github.com/tpope/vim-surround.git'           => 'vim/bundle',
    'https://github.com/tpope/vim-unimpaired.git'         => 'vim/bundle',
    'https://github.com/tpope/vim-vinegar.git'	          => 'vim/bundle',
    'https://github.com/altercation/vim-colors-solarized' => 'vim/bundle',
);

my @Web = (
    {
        'source' =>
          'http://www.vim.org/scripts/download_script.php?src_id=13152',
        'dest' => 'vim/colors',
        'name' => 'fu.vim'
    },
);

my %Config = (
    'vimrc'      => 'config',
    'bashrc'     => 'config',
    'install.sh' => 'home',
);

# Get an array of all directories to create
foreach my $k ( keys %Git ) {
    push @tmpdirs, "$basedir/$Git{$k}";
}

for ( 0 .. $#Web ) {
    push @tmpdirs, "$basedir/$Web[$_]->{dest}";
}

foreach my $k ( keys %Config ) {
    push @tmpdirs, "$basedir/$Config{$k}";
}

@dirs = uniq @tmpdirs;

#-------------------------------------------------------#
# clean old files
say colored( "Cleaning old files:", "bright_green" );

unlink $outfile
  or warn "Could not unlink $outfile: $!";
my $rmtree = rmtree("$basedir")
  or warn "Could not delete $basedir $!";
say "NUmber of files deleted in $basedir $rmtree\n";

#create directories
say colored( "Creating directories", "bright_green" );

my @created = mkpath(
    @dirs,
    {
        verbose => 1,
        mode    => 0711,
    }
);

say colored( 'Cloning git repos...', "bright_green" );
## Download git repos
my $cwd = cwd();
foreach my $k ( keys %Git ) {
    chdir("$basedir/$Git{$k}");
    Git::Repository->run( clone => $k );

    #say $GitError;
    chdir($cwd);
}

# Download fu colorscheme
for ( 0 .. $#Web ) {
    my $url = $Web[$_]->{source};
    say "Downloading: $url";
    say "  to: $basedir/$Web[$_]->{dest}/$Web[$_]->{name}";

    #	my $download =
    getstore( $url, "$basedir/$Web[$_]->{dest}/$Web[$_]->{name}" )
      or warn "Download of $url failed: $!";

    #	say $download;
}

say colored( 'Cloning repos DONE.', "bright_green" );

say colored( "copying files to config", "bright_green" );
cp( "vimrc", "$basedir/config/vimrc" )
  or die "Copy failed: $!";
cp( "bashrc", "$basedir/config/bashrc" )
  or die "Copy failed: $!";
cp( "install.sh", "$basedir/install.sh" )
  or die "Copy failed: $!";

## -----------creating archive --------------
say colored( "Creating archive", "bright_green" );

# make a list of all files in $basedir
find( \&archive_files, "$basedir" );

sub archive_files {
    push @files, "$File::Find::name";
}

# Create the archive
Archive::Tar->create_archive( "$outfile", 9, @files );

say colored( "All done.", "bright_yellow" );
# vim: set ts=4 sts=4 sw=4 expandtab :
