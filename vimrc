" Enabling pathogen
execute pathogen#infect()

" settings
syntax on
filetype plugin indent on
set nocompatible

" If LC_SOLARIZED is 'true' it means putty session has solarized colors
let solarizedPutty=$LC_SOLARIZED

if solarizedPutty != 'true'
	let g:solarized_termcolors=256	
endif

" Color settings
colorscheme solarized
set background=dark
