#!/bin/bash

echo "Setting up defaults"

if [[ -e ~/.bashrc ]]; then 
	if grep -Fxq "$(cat config/bashrc)" ~/.bashrc
	then
		echo "Content exists, wont add a second time"
	else
		echo "Appending to ~/.bashrc"
		cat config/bashrc >> ~/.bashrc
		echo "  done."
	fi

else
	echo "No .bashrc exists. Creating a minimal one."
	cp config/bashrc-minimal ~/.bashrc
	cat config/bashrc >> ~/.bashrc
	echo "  done."
fi

if [[ ! -d ~/dircolors-solarized ]]; then
	echo "Copying dircolors to ~"
	cp -r home/dircolors-solarized ~/dircolors-solarized
	echo "  done."
else
	echo "dircolors already installed, skipping copy"
fi	

if [[ -e ~/.vimrc ]]; then
	echo "vimrc exists. renaming existing vimrc to .vimrc-orig"
	mv ~/.vimrc ~/.vimrc-orig
	echo "  done."
	echo "Copying std vimconfig to ~/.vimrc"
	cp config/vimrc ~/.vimrc
	echo "  done."
else
	echo "Copying std vimconfig to ~/.vimrc"
	cp config/vimrc ~/.vimrc
	echo "  done."
fi

if [[ -d ~/.vim ]]; then
	cp -a vim ~/.vim
else
	if [[ ! -e ~/.vim/autoload/pathogen.vim ]]; then
		echo "Installing Tpopes pathogen"
		mkdir -p ~/.vim/autoload/
		cp vim/autoload/vim-pathogen/autoload/pathogen.vim ~/.vim/autoload/
		echo "  done."
	fi

	if [[ ! -d ~/.vim/bundle/ ]]; then
		echo "creating ~/.vim/bundle"
		mkdir -p ~/.vim/bundle
		echo "  done."
	fi
	if [[ ! -d ~/.vim/colors/ ]]; then
		echo "creating ~/.vim/colors"
		mkdir -p ~/.vim/colors
		echo "  done."
	fi

	echo "copying vim plugins"
	cp -r vim/bundle/vim-sensible ~/.vim/bundle/
	cp -r vim/bundle/vim-surround ~/.vim/bundle/
	cp -r vim/bundle/vim-unimpaired ~/.vim/bundle/
	cp -r vim/bundle/vim-colors-solarized ~/.vim/bundle/
	cp vim/colors/fu.vim ~/.vim/colors/
fi

echo "  done."


echo ""
echo ""
echo "##### All done. log out and in again #####"
echo ""
echo ""

